package exercise;

import java.util.Iterator;

/**
 * @author glor
 */
public class RBTIterator<T extends Comparable<T>> implements Iterator<T> {
	/**
	 * 5 Directions
	 * 0: entering tree, going left
	 * 1: leaving left subtree/printing out
	 * 2: going right
	 * 3: leave right subtree
	 * 4: finished
	 */
	private int direction = 0;
	private RedBlackTree<T> tree;
	private Node<T> root = null;
	protected Node<T> currentElem = null;
	/**
	 * default Iterator-Constructor
	 */
	public RBTIterator(RedBlackTree<T> tree) {
		this.tree = tree;
		root = tree.root;
		currentElem = root;
		/**
		 * going to smallest number
		 */
		while(hasLeft()) {
			currentElem = getLeft();
		}
	}
	public boolean hasNext() {
		// finished?
		if(direction == 4) {
			return false;
		}
		/**
		 * are we at the largest child
		 */
		Node<T> lastElem = root;
		while(lastElem.hasRight()) {
			lastElem = lastElem.getRight();
		}
		/**
		 * did we already return last Child
		 */
		return (currentElem == lastElem & direction>1);
	}
	public T next() {
		while(direction != 4) {
			switch (direction) {
			case 0:
				if(hasLeft()) {
					currentElem = getLeft();
					break;
				}
				direction++;
				break;
			case 1:
				direction++;
				return currentElem.getValue();
			case 2:
				if(hasRight()) {
					direction = 0;
					break;
				}
				direction++;
				break;
			case 3:
				if(currentElem.getParent()==null) {
					direction = 4;
					throw new RuntimeException("End of iteration.");
				}
				if(getParentgetLeft()==currentElem) {
					direction = 1;
					currentElem = currentElem.getParent();
				}
				break;
			}
		}
		return null;
	}
	
	protected boolean hasLeft() {
		return currentElem.hasLeft();
	}
	protected boolean hasRight() {
		return currentElem.hasRight();
	}
	protected Node<T> getLeft() {
		return currentElem.getLeft();
	}
	protected Node<T> getRight() {
		return currentElem.getRight();
	}
	protected Node<T> getParentgetLeft() {
		return currentElem.getParent().getLeft();
	}
	
	public void remove() {
		tree.remove(currentElem);
	}

}
