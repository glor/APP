package exercise;

final class Node<T> {
	private T value = null;
	private boolean color = false;
	private Node<T> parent = null;
	private Node<T> right = null;
	private Node<T> left = null;
	/**
	 * Root-Node
	 */
	public Node(T value) {
		this.setValue(value);
	}
	public Node(Node<T> parentNode, T value) {
		this.parent = parentNode;
		this.setValue(value);
	}
	/**
	 * getter
	 */
	public T getValue() {
		return value;
	}
	public void setValue(T value) {
		this.value = value;
	}
	/**
	 * getter
	 */
	public boolean getColor() {
		return color;
	}
	public void setColor(boolean color) {
		this.color = color;
	}
	/**
	 * getter
	 */
	public Node<T> getParent() {
		return parent;
	}
	public void setParent(Node<T> parent) {
		this.parent = parent;
	}
	/**
	 * getter
	 */
	public Node<T> getLeft() {
		return left;
	}
	public void setLeft(Node<T> left) {
		this.left = left;
	}
	/**
	 * getter
	 */
	public Node<T> getRight() {
		return right;
	}
	public void setRight(Node<T> right) {
		this.right = right;
	}
	/**
	 * getter
	 */
	public boolean hasLeft() {
		return left!=null;
	}
	/**
	 * getter
	 */
	public boolean hasRight() {
		return right!=null;
	}
	/**
	 * recursive toString()
	 * ~O(n^3)
	 */
	public String toString() {
		String out = "";
		if(hasLeft()) {
			out += left.toString() + " ";
		}
		out += value.toString();
		if(hasRight()) {
			out += " " + right.toString();
		}
		return out + " ";
	}
}
