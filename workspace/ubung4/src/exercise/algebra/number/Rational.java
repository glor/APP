package exercise.algebra.number;

/**
 * @author Lorenz Glissmann
 */
/**  Class for Rational Numbers */
public class Rational {
	static public Rational ZERO = new Rational();
	/** get Sum of 2 Rational */
	static public Rational add(Rational x, Rational y) {
		Rational c = new Rational(x);
		c.add(y);
		return c;
	}

	/** get Product of 2 Rational */
	static public Rational mul(Rational x, Rational y) {
		Rational c = new Rational(x);
		c.mul(y);
		return c;
	}

	/** Check 2 Rational for Equality */
	static public boolean equals(Rational x, Rational y) {
		return x.equals(y);
	}

	/** get HashCode of a Rational */
	static public int getHashOf(Rational r) {
		return r.hashCode();
	}

	private long num = 0; // Zähler
	private long denum = 0; // Nenner

	static public Rational invert(Rational r) {
		Rational tmp = new Rational(r);
		tmp.invert();
		return tmp;
	}
	/** get Maximum */
	static public Rational max(Rational x, Rational y) {
		if (x.getNum() / x.getDenum() == y.getNum() / y.getDenum()) {
			if (x.getNum() % x.getDenum() == y.getNum() % y.getDenum()) {
				return x;
			}
			if (x.getNum() % x.getDenum() > y.getNum() % y.getDenum()) {
				return x;
			} else {
				return y;
			}
		}
		if (x.getNum() / x.getDenum() > y.getNum() / y.getDenum()) {
			return x;
		} else {
			return y;
		}
	}

	/**  Default-Constructor always returns ZERO */
	Rational() {
	}

	/**  Initialize as a Copy */
	Rational(Rational c) {
		setNum(c.getNum());
		setDenum(c.getDenum());
	}

	/**  Initilize with given num- and denum part */
	Rational(long num, long denum) {
		setNum(num);
		setDenum(denum);
	}
	
	/**  Initilize with Integer */
		Rational(long num) {
			setNum(num);
			setDenum(1);
		}

	/**  get num part of current Rational Number */
	public long getNum() {
		return num;
	}

	/**  set num part of current Rational Number */
	public void setNum(long num) {
		this.num = num;
	}

	/**  get denum part of current Rational Number */
	public long getDenum() {
		return denum;
	}

	/**  set denum part of current Rational Number */
	public void setDenum(long denum) {
		if(denum == 0) {
			if(denum!=0) {
				System.exit(1);
			}
		}
		this.denum = denum;
	}

	/**  operations on Rational:
	 * Adding: to current Object */
	public void add(Rational c) {
		num += c.getNum();
		denum += c.getDenum();
	}

	/**  Multiplying current Object with given Object */
	public void mul(Rational c) {
		setNum(num * c.getNum());
		setDenum(denum * c.getDenum());
	}

	/**  return string representation */
	public String toString() {
		return num + "/" + denum;
	}

	/**  duplicates the object and returns new reference */
	public Object clone() {
		return new Rational(num, denum);
	}

	/**  Check for Equality of Objects with current Object */
	public boolean equals(Object o) {
		/*
		 * x.equals(null) := false It is reflexive: x.equals(x) should return
		 * true. It is symmetric: x.equals(y) <=> y.equals(x) It is transitive:
		 * a == x.equals(y) == y.equals(z) => x.equals(z) == a It is consistent:
		 * always behaves the same way
		 */
		if (o == null) {
			return false;
		}
		if (this == o) { // the same reference
			return true;
		}
		if (!(o instanceof Rational)) { // different class
			return false;
		}
		Rational c = (Rational) o; // casting to Rational so o's vars can be
									// used

		// Object Equality <=> num and denum part are equal

		return (num * c.getDenum() == c.getNum() * denum);
	}

	public void invert() {
		long tmp;
		tmp = num;
		setNum(denum);
		setDenum(tmp);
	}

	/**  get Hash of current Rational Number */
	public int hashCode() {
		// for equal Objects hashCode is also equal
		// every change in input changes the hash
		long tmp = num ^ denum;
		return (int) tmp ^ (int) (tmp >> 32);
	}

	public double getDouble() {
		return (double) num / (double) denum;
	}
}