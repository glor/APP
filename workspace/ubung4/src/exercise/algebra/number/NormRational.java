package exercise.algebra.number;

/**
 * @author Lorenz Glissmann
 */

/**
 * Class for Rational Numbers in Normal Form
 */
public class NormRational extends Rational {
	/**
	 * default constructor
	 */
	public NormRational() {
		super();
	}
	/**
	 * copy constructor for Rational
	 */
	public NormRational(Rational c) {
		super(c);
	}
	/**
	 * copy constructor
	 */
	public NormRational(NormRational c) {
		super(c);
	}
	/**
	 * initializing Integer constructor
	 */
	public NormRational(long num) {
		super(num);
	}
	/**
	 * initializing constructor
	 */
	public NormRational(long num, long denum) {
		super(num, denum);
	}
	private void normalize() {
		long ggt = ggt(getDenum(), getNum());
		super.setNum(getNum() / ggt);
		super.setDenum(getDenum() / ggt);
	}
	public void setNum(long num) {
		super.setNum(num);
		normalize();
	}
	public void setDenum(long denum) {
		super.setDenum(denum);
		normalize();
	}
	private static long ggt(long x, long y) {
		while(y != 0) {
			if(x>y) {
				x = x - y;
			} else {
				y = y - x;
			}
		}
		return x;
	}
}
