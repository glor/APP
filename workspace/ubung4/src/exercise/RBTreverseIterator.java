package exercise;

public class RBTreverseIterator<T extends Comparable<T>> extends RBTIterator<T> {

	public RBTreverseIterator(RedBlackTree<T> tree) {
		super(tree);
	}
	/**
	 * Override for all the getters and setters.
	 * Change order by inverting tree
	 */
	protected boolean hasLeft() {
		return currentElem.hasRight();
	}
	protected boolean hasRight() {
		return currentElem.hasLeft();
	}
	protected Node<T> getLeft() {
		return currentElem.getRight();
	}
	protected Node<T> getRight() {
		return currentElem.getLeft();
	}
	protected Node<T> getParentgetLeft() {
		return currentElem.getParent().getRight();
	}
}
