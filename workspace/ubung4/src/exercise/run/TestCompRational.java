package exercise.run;

import java.util.Iterator;
import java.util.Scanner;

import exercise.RedBlackTree;
import exercise.algebra.number.CompRational;

/**
 * @author Lorenz Glißmann
 */
public class TestCompRational {
	public static void main(String[] args) {
		long x, y;
		Scanner scn = new Scanner(System.in);
		RedBlackTree<CompRational> rbt = new RedBlackTree<CompRational>();
		while(scn.hasNextInt()) {
			x = scn.nextInt();
			y = scn.nextInt();
			if(x==0 && y==0) {
				break;
			}
			rbt.add(new CompRational(x,y));
			
		}
		Iterator<CompRational> it = rbt.iterator();
		while(it.hasNext()) {
			System.out.print(it.next() + " ");
		}
		System.out.println();
		/**
		 * reinit
		 */
		it = rbt.iterator();
		while(it.hasNext()) {
			if(it.next().compareTo(CompRational.ONE)>0) {
				it.remove();
			}
		}
		/**
		 * reinit
		 */
		it = rbt.iterator();
		while(it.hasNext()) {
			System.out.print(it.next() + " ");
		}
		System.out.println();
		scn.close();
	}

}
