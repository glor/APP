package exercise;

import java.util.Iterator;
import java.util.Stack;

public class StackIterators<T extends Comparable<T>> implements Iterator<T> {
	
	Stack<Node<T>> s = new Stack<Node<T>>();
	RedBlackTree<T> tree;
	Node<T> current;
	
	StackIterators(RedBlackTree tree) {
		this.tree = tree;
		addNodes(tree.root);
	}
	
	public void addNodes(Node<T> x) {
		s.push(x);
		while(x.hasLeft()) {
			x = x.getLeft();
			s.push(x);
		}
	}
	public boolean hasNext() {
		return !s.isEmpty();
	}
	public T next() {
		current = s.pop();
		if(current.hasRight()) {
			addNodes(current.getRight());
		}
		return current.getValue();
	}
	public void remove() {
		tree.(current);
	}
}
