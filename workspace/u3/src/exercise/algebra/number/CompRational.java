package exercise.algebra.number;

public class CompRational extends NormRational implements Comparable<CompRational> {
	/**
	 * default constructor
	 */
	public CompRational() {
		super();
	}
	/**
	 * copy constructor for Rational
	 */
	public CompRational(Rational c) {
		super(c);
	}
	/**
	 * copy constructor
	 */
	public CompRational(NormRational c) {
		super(c);
	}
	/**
	 * initializing Integer constructor
	 */
	public CompRational(long num) {
		super(num);
	}
	/**
	 * initializing constructor
	 */
	public CompRational(long num, long denum) {
		super(num, denum);
	}
	/**
	 * comparision method
	 */
	public int compareTo(CompRational o) {
		/**
		 * Equality
		 */
		if(equals(o)) {
			return 0;
		}
		/**
		 * if one number is negativ but not the other then its clearly smaller
		 */
		if(getNum()<0 && o.getNum()>=0) {
			return -1;
		}
		if(o.getNum()<0 && getNum()>=0) {
			return 1;
		}
		/** Da in java
		 * -x mod y == -(x mod y)
		 * kann max() benutzt werden */
		CompRational tmp = new CompRational(getNum(), getDenum());
		if(Rational.max(o, tmp) == o) {
			return -1;
		} else {
			return 1;
		}
	}
}
