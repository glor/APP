package exercise.adt;

import exercise.algebra.number.CompRational;

public class BinarySearchTree {
	private CompRational value;
	
	private boolean hasLeft() {
		return (left != null);
	}
	private boolean hasRight() {
		return (right != null);
	}
	public BinarySearchTree getLeft() {
		return left;
	}
	public BinarySearchTree getRight() {
		return right;
	}
	public void setLeft(BinarySearchTree child) {
		left = child;
	}
	public void setRight(BinarySearchTree child) {
		right = child;
	}
	private BinarySearchTree left = null;
	private BinarySearchTree right = null;
	public BinarySearchTree() {
		value =  new CompRational();
	}
	public BinarySearchTree(CompRational value) {
		this.value =  new CompRational(value);
	}
	public BinarySearchTree(CompRational value, BinarySearchTree child1, BinarySearchTree child2) {
		this.value =  new CompRational(value);
	}
	public void insert(CompRational child) throws AlreadyInTheTreeException {
		switch(value.compareTo(child)) {
		case 1:
			if(!hasLeft()) {
				setLeft(new BinarySearchTree(child));
			} else {
				getLeft().insert(child);
			}
		case -1:
			if(!hasRight()) {
				setRight(new BinarySearchTree(child));
			} else {
				getRight().insert(child);
			}
		default:
			throw new AlreadyInTheTreeException();
		}
	}
	private BinarySearchTree find(CompRational r) throws NotInTheTreeException {
		switch(value.compareTo(r)) {
		case 1:
			if(!hasLeft()) {
				throw new NotInTheTreeException();
			} else {
				return getLeft().find(r);
			}
		case -1:
			if(!hasRight()) {
				throw new NotInTheTreeException();
			} else {
				return getLeft().find(r);
			}
		default:
			return this;
		}
	}
	public CompRational getValue() {
		return value;
	}
	public void remove(CompRational r) throws NotInTheTreeException, AlreadyInTheTreeException {
		BinarySearchTree bst = find(r);
		BinarySearchTree oldLeft = bst.getLeft();
		BinarySearchTree oldRight = bst.getRight();
		bst = null;
		if(oldLeft != null) {
			reinsert(oldLeft, this);
		}
		if(oldRight != null) {
			reinsert(oldRight, this);
		}
	}
	private void reinsert(BinarySearchTree r, BinarySearchTree root) throws AlreadyInTheTreeException {
		if(r.hasLeft()) {
			reinsert(r.getLeft(), root);
		}
		if(r.hasRight()) {
			reinsert(r.getRight(), root);
		}
		root.insert(value);
	}
	/*private BinarySearchTree leftest() {
		if(hasLeft()) {
			return left.leftest();
		} else {
			return this;
		}
	}*/
	public String toString() {
		String out = "";
		if(hasLeft()) {
			out += left.toString() + " ";
		}
		out += value.toString();
		if(hasRight()) {
			out += " " + right.toString();
		}
		return out + " ";
	}
}
