package exercise.run;

import java.io.*; 
import java.util.Stack;

import exercise.algebra.number.NormRational;
import exercise.algebra.number.Rational;

public class RPN {
	public static void main(String args[]) {
		int i;
		char c;
		String nbr = "";
		Stack<NormRational> numbers = new Stack<NormRational>();
		NormRational x;
		NormRational y;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		// read characters 
		try {
			while((i=br.read())!=-1){ 
				c = (char)i;
				if(c==' ') {
					if(nbr=="") {
						continue;
					}
					if(nbr.matches("[0-9]+")) {
						numbers.push(new NormRational(Long.parseLong(nbr)));
						nbr = "";
						continue;
					}
					throw new InputFormatException();
				}
				if(c=='+') {
					if(numbers.size()<2) {
						throw new InputFormatException();
					}
					x = numbers.pop();
					y = numbers.pop();
					numbers.push(new NormRational(Rational.add(x, y)));
				}
				if(c=='-') {
					if(numbers.size()<2) {
						throw new InputFormatException();
					}
					x = numbers.pop();
					y = numbers.pop();
					y.setNum(y.getDenum());
					numbers.push(new NormRational(Rational.add(x, y)));
				}
				if(c=='*') {
					if(numbers.size()<2) {
						throw new InputFormatException();
					}
					x = numbers.pop();
					y = numbers.pop();
					numbers.push(new NormRational(Rational.mul(x, y)));
				}
				if(c=='/') {
					if(numbers.size()<2) {
						throw new InputFormatException();
					}
					x = numbers.pop();
					y = numbers.pop();
					if(y.getDenum()==0) {
						throw new InputFormatException("Divide by 0");
					}
					y.invert();
					numbers.push(new NormRational(Rational.mul(x, y)));
				}
				nbr += c;
			}
			if(numbers.size()!=1) {
				throw new InputFormatException();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InputFormatException e) {
			System.exit(1);
		}
		System.out.printf("%s\n", numbers.pop().toString());
	}
	

}
