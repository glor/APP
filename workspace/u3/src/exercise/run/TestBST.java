package exercise.run;

import java.util.Scanner;

import exercise.adt.*;
import exercise.algebra.number.CompRational;

public class TestBST {
	/**
	 * Test Class
	 */
	public static void main(String[] args) {
		long x, y;
		Scanner scn = new Scanner(System.in);
		BinarySearchTree bst = new BinarySearchTree();
		while(scn.hasNextInt()) {
			x = scn.nextInt();
			y = scn.nextInt();
			if(x==0 && y==0) {
				break;
			}
			try {
				bst.insert(new CompRational(x,y));
			} catch (AlreadyInTheTreeException e) {
				e.printStackTrace();
			}
		}
		System.out.printf(bst.toString());
		while(scn.hasNextInt()) {
			x = scn.nextInt();
			y = scn.nextInt();
			try {
				bst.remove(new CompRational(x,y));
			} catch (NotInTheTreeException e) {
				e.printStackTrace();
			} catch (AlreadyInTheTreeException e) {
				e.printStackTrace();
			}
		}
		System.out.printf(bst.toString());
	}
}
