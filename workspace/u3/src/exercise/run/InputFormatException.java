package exercise.run;

//import java.lang.Exception;

public class InputFormatException extends Exception {

	public InputFormatException(String str) {
		System.out.printf("%s", str);
	}

	public InputFormatException() {
		System.out.printf("Wrong input Format!\n");
	}
	
}
