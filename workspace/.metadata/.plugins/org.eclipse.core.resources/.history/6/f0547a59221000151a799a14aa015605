package exercise;

import java.util.AbstractCollection;
import java.lang.Comparable;
import java.util.Iterator;

import exercise.AlreadyInTheTreeException;
import exercise.NotInTheTreeException;

/**
 * @author Lorenz Glißmann
 */

public class RedBlackTree<T extends Comparable<T>> extends AbstractCollection<T> {
	
	public final boolean RED = false;
	public final boolean BLACK = true;
	
	private int size = 0;
	private int depth = 0;
	protected Node<T> root;
	
	public Iterator<T> iterator() {
		return new RBTIterator<T>(this);
	}
	
	public Iterator<T> reverseIterator() {
		return new RBTreverseIterator<T>(this);
	}

	public int size() {
		return size;
	}
	protected void decSize() {
		size--;
	}
	public void insert() {
		size++;
	}
	public void remove() {
		
		size--;
	}
	
	/*public boolean addAll(AbstractCollection<T> c) {
		Iterator<T> i = c.iterator();
		boolean out = false;
		while(i.hasNext()) {
			if( add(i.next()) ) {
				out = true;
			}
		}
	}*/
	public boolean add(T t) {
		Node<T> n = root;
		int i;
		while( (i=n.getValue().compareTo(t)) != 0 ) {
			if(i==1) {
				if(!n.hasLeft()) {
					n.setLeft(new Node<T>(t));
					return true;
				} else {
					n = n.getLeft();
				}
			} else {
				if(!n.hasRight()) {
					n.setRight(new Node<T>(t));
					return true;
				} else {
					n = n.getRight();
				}
			}
		}
		return false;
	}
	private void insertRepair(Node<T> n) {
		/**
		 * Fall 1
		 */
		if(n==root) {
			return;
		}
		if(n.getParent().getColor()==BLACK) {
			return;
		}
		/**
		 * Fall 2
		 */
		if(n.getParent()==root && root.getColor()==RED) {
			root.setColor(BLACK);
			return;
		}
		/**
		 * Fall 3
		 */
		if(n.getParent().getParent().getLeft().getColor()==RED && n.getParent().getParent().getRight().getColor()==RED) {
			n.getParent().getParent().setColor(RED);
			n.getParent().getParent().getLeft().setColor(BLACK);
			n.getParent().getParent().getRight().setColor(BLACK);
			insertRepair(n.getParent().getParent());
			return;
		}
		/**
		 * Fall 4
		 * Schwarzer Vater wurde bereits in Fall 1 abgedeckt 
		 * => Vater rot
		 */
		if(n.getParent().getParent().getLeft().getColor() ^ n.getParent().getParent().getRight().getColor()) {
			/**
			 * 1.
			 */
			if( isLeftChild(n.getParent()) && !isLeftChild(n) ) {
				rotateLeft(n.getParent());
				n = n.getParent();
			}
			/**
			 * 3.
			 * Vater Rot
			 */
			if( isLeftChild(n.getParent()) && isLeftChild(n) ) {
				n.getParent().setColor(n.getParent().getParent().getColor());
				n.getParent().getParent().setColor(RED);
				rotateRight(n.getParent().getParent());
				return;
			}
			/**
			 * 2.
			 */
			if( !isLeftChild(n.getParent()) && isLeftChild(n) ) {
				rotateRight(n.getParent());
				n = n.getParent();
			}
			/**
			 * 4.
			 */
			if( !isLeftChild(n.getParent()) && !isLeftChild(n) ) {
				n.getParent().setColor(n.getParent().getParent().getColor());
				n.getParent().getParent().setColor(RED);
				rotateLeft(n.getParent().getParent());
				return;
			}
		}
	}
	public boolean isLeftChild(Node<T> n) {
		if(n.getParent()==null) {
			throw new NullPointerException();
		}
		return (n.getParent().getLeft()==n);
	}
	public boolean remove(T t) {
		/**
		 * Iterator for binary search
		 */
		Node<T> n = root;
		while(true) {
			switch(n.getValue().compareTo(t)) {
			case 1:
				if(!n.hasLeft()) {
					return false;
				} else {
					n = n.getLeft();
				}
			case -1:
				if(!n.hasRight()) {
					return false;
				} else {
					n = n.getRight();
				}
			case 0:
				purge(n);
				size--;
				return true;
			}
		}
	}
	private void purge(Node<T> n) {
		/**
		 * Fall 1
		 */
		/**
		 * ist n ein Blatt?
		 */
		if(!n.hasRight()) {
			/**
			 * Elternreferenz töten
			 */
			if(n.getParent().getLeft()==n) {
				n.getParent().setLeft(n.getRight());
			} else {
				n.getParent().setRight(n.getRight());
			}
		} else {
			/**
			 * suche linkesten unterknoten
			 */
			Node<T> m = n.getRight();
			while(m.hasLeft()) {
				m = m.getLeft();
			}
			/**
			 * Value-change schützt vor NULL-Pointer Exception.
			 * ggf. Linker unterbaum bleibt erhalten.
			 */
			n.setValue(m.getValue());
			/**
			 * rechter unterbaum vom m wird hochgezogen
			 */
			if(m.hasRight()) {
				/**
				 * Elternreferenz zwischenspeichern
				 */
				Node<T> p = m.getParent();
				if(m.getParent().getLeft()==m) {
					m.getParent().setLeft(m.getRight());
				} else {
					m.getParent().setRight(m.getRight());
				}
				m.setParent(p);
			}
			
		}
		if(n.getParent().getLeft()==n) {
			n.setLeft(null);
		} else {
			
		}
		return;
	}
	private Node<T> rightLeftest(Node<T> n) {
		if(!n.hasRight()) {
			return null;
		}
		n = n.getRight();
		while(n.hasLeft()) {
			n = n.getRight();
		}
		return n;
	}
	public int blackDepth() {
		return 0;
	}
	/**
	 * Linksrotation
	 */
	protected void rotateLeft(Node<T> x) {
		Node<T> y = x.getRight();
		if(x==x.getParent().getLeft()) {
			x.getParent().setLeft(y);
		} else {
			x.getParent().setRight(y);
		}
		y.setParent(x.getParent());
		x.setParent(y);
		x.setRight(y.getLeft());
		y.setLeft(x);
		x.getRight().setParent(x);
	}
	/**
	 * Rechtsrotation
	 */
	protected void rotateRight(Node<T> x) {
		Node<T> y = x.getLeft();
		if(x==x.getParent().getRight()) {
			x.getParent().setRight(y);
		} else {
			x.getParent().setLeft(y);
		}
		y.setParent(x.getParent());
		x.setParent(y);
		x.setRight(y.getRight());
		y.setRight(x);
		x.getLeft().setParent(x);
	}
	public void remove(Node<T> n) {
		size--;
	}
}
